# Gym 13 End user documentation

Whether you are a long time user of our services or a new member, this manual will help you understand how to use our application. 

## Why an application

We belive that you need to be in power of your gym experience, interacting with a human will only slow down your workout. 


## How to use
Interaction with the systeme are done in the java Console. The application require no installation at the moment and it can be executed by CLI. 

**Arriving at the gym**: When you arrive you should authentificate in the app to get a custum QR code permitting access to the installation. Should you get an error, please check with the agent at the door you may have a pending credit in your account. 

**Enrolling in a session**: Want to do cardio with one of our professional? Head up to the enroll in session menu and you can get a view of all available session for the day. After selecting a sesssion you will receive a confirmation number. This will be used to validate your inscription and should be shown to the professional for him to scan **before** joining the session. In that way, it is possible to keep track of who participated.

**Pay your services**: You receive an invoice each week with the services consumed. You have 1 month to pay the invoice. If the invoice is not paid, the account become innactive and access to the gym might be refused. To pay your invoice please refer to RnB as they are the payment gateway provider.


Sign in is done via Facebook. If the user doesn't have Facebook he can [sign up](https://www.facebook.com/) . If he forgot his facebook password he is invited to check [the facebook recovery tool](https://www.facebook.com/recover/initiate/).

----
## Permission

Each user type have different permission. They are as follow:

1. **The agent**
    > is the frontline worker helping the user in the registration process, he is the one that create new sessions.
    
    - He can create a new user or professional profile.
    - He can create a new service and generate the appropriate sessions

2. **The Client**
    > is the member of #gym, he is at the center of the system, interacting with services and professional
    - He can update his profile
    - To log in, he needs to provide a valid email
    - He has the possibility to delete his account
    - He can subscribe to sessions
    - He can auto check in #gym by creating a QR code
    - He can access his bills
    - He can generate a QR code to show the professional upon entry

3. **The Professional**
    > is the provider of services, he interacts with the agent to create session and also with clients to validate the provision of service
    
    - He can check his upcoming sessions
    - He can check his due invoices
    - He can check in the client to a session
    - He can modify a service
    - He can delete a service
    - He can modify and delete his profile

    
4. **The Manager**
    > He is the boss that generate reports
    - He can manualy generate reports


---
Should you have any questions on the system, please dont hesitate contacting the developpers.