package com.team13;

import java.util.Scanner;

import com.team13.models.ClientRepository;
import com.team13.views.MainMenu;
import com.team13.views.dumbData;
import com.team13.models.*;

public class App {
    // iniate all repo
    public static ClientRepository allClient = new ClientRepository();
    public static ProfessionalRepository allProfessional = new ProfessionalRepository(); // initiate the repository
    private static Scanner sc = new Scanner(System.in);

    // 1- Iniate the repos
    // a. Client Repo
    // b. Pro Repo
    // c. Service Repo
    // (d. Session Repo)
    // 2 - Create some dumb data clientRepo.add
    // 3- Invoke the main menu

    public static void main(String[] args) {

        dumbData.generateData();

        System.out.println("Welcome to #GYM self service system");
        MainMenu.displayMenu();

        sc.close();

    }
}
