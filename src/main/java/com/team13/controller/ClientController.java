package com.team13.controller;

import com.team13.models.Client;
import com.team13.App;
import com.team13.models.ClientRepository;

/**
 * 1- Authentificate 2- show QR code (Check if member is active)
 */

public class ClientController {

    /**
     * Method to check if client can enter the gym
     * 
     * @param clientID is the client ID to check
     * @return Invalid. Invalid ID if ID is not found Invalid. Suspended Member if
     *         member has not paid Valid if member is valid
     */
    public static String enterGym(Client client) {
        if (client.getHasPaid() == false) {
            // client has not paid but is valid
            return "Invalid. Suspended Member";
        } else {
            return "Valid";
        }
    }

    // used to get the client object, should be used with valid ID only
    public static Client getClientObject(String ValidclientID) {
        try {
            ClientRepository clientRepo = new ClientRepository();
            Client foundClient = clientRepo.searchClientByID(ValidclientID);
            if (foundClient != null) {
                return foundClient;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
