package com.team13.controller;

import com.team13.models.Service;
import com.team13.models.ServiceRepository;
import com.team13.models.Session;

import java.util.Scanner;

public class ServiceController extends ServiceRepository {

	private static Scanner sc = new Scanner(System.in);
	private String nl;
	private boolean validInput;

	/*
	 * create a service by putting in information
	 */
	public Service createService() {

		// set client ID
		String prof;
		do {
			validInput = true;
			System.out.print("\nenter client ID : ");
			try {
				nl = sc.nextLine();
				Integer.parseInt(nl);
				if (nl.length() != 9) {
					System.out.println("\n***please enter valid client ID***");
					validInput = false;
				}
			} catch (NumberFormatException ex) {
				validInput = false;
				System.out.println("\n***please enter numbers only***");
			}
		} while (!validInput);
		prof = nl;

		// set service name
		String name;
		do {
			validInput = true;
			System.out.print("\nenter service name : ");
			name = sc.nextLine();
			if (name.length() == 0 || name.length() > 20) {
				validInput = false;
				System.out.println("\n***min 1 character, max 20 characters***");
			}
		} while (!validInput);

		// set start date
		int start = 0;
		do {
			validInput = true;
			System.out.print("\nenter start date : ");
			try {
				nl = sc.nextLine();
				start = Integer.parseInt(nl);
				if (nl.length() != 4) {
					System.out.println("\n***please enter valid start date(MMDD)***");
					validInput = false;
				}
			} catch (NumberFormatException ex) {
				validInput = false;
				System.out.println("\n***please enter numbers only***");
			}
		} while (!validInput);

		// set end date
		int end = 0;
		do {
			validInput = true;
			System.out.print("\nenter end date : ");
			try {
				nl = sc.nextLine();
				end = Integer.parseInt(nl);
				if (nl.length() != 4) {
					System.out.println("\n***please enter valid end date(MMDD)***");
					validInput = false;
				}
			} catch (NumberFormatException ex) {
				validInput = false;
				System.out.println("\n***please enter numbers only***");
			}
		} while (!validInput);

		// set description
		String comment;
		do {
			validInput = true;
			System.out.print("\nenter description : ");
			comment = sc.nextLine();
			if (comment.length() > 100) {
				validInput = false;
				System.out.println("\n***max 100 characters***");
			}
		} while (!validInput);

		// set fee
		float fee = 0;
		do {
			validInput = true;
			System.out.print("\nenter fee : ");
			try {
				nl = sc.nextLine();
				fee = Float.parseFloat(nl);
				if (fee < 0) {
					System.out.println("\n***please enter positive fee***");
					validInput = false;
				}
			} catch (NumberFormatException ex) {
				validInput = false;
				System.out.println("\n***please enter numbers only***");
			}
		} while (!validInput);

		// set capacity
		int cap = 1;
		do {
			validInput = true;
			System.out.print("\nenter capacity : ");
			try {
				nl = sc.nextLine();
				cap = Integer.parseInt(nl);
				if (cap <= 0) {
					validInput = false;
					System.out.println("\n***please enter positive capacity***");
					cap = 1;
				}
			} catch (NumberFormatException ex) {
				validInput = false;
				System.out.println("\n***please enter numbers only***");
			}
		} while (!validInput);

		// create service
		Service newService = new Service(prof, name, start, end, comment, fee, cap);
		return newService;
	}

	/*
	 * put new service in service repository
	 */
	public void finalizeCreation(Service newService) {
		Create(newService);
	}

	/*
	 * print out options
	 */
	public void updateMenu() {
		System.out.println("\n===Update Menu===\n");
		System.out.println("1 - update service name");
		System.out.println("2 - update start date");
		System.out.println("3 - update end date");
		System.out.println("4 - update description");
		System.out.println("5 - update fee");
		System.out.println("6 - update capacity");
		System.out.println("7 - add session");
		System.out.println("8 - delete session");
		System.out.println("0 - back to service menu");
		System.out.println("\nenter choice : ");
	}

	/*
	 * take input service ID to get the service requested then take input option to
	 * update information
	 */
	public void updateService(int serviceid) {
		Service service = getService(String.valueOf(serviceid));
		if (service != null) {
			int option = 0;
			do {
				do {
					validInput = true;
					updateMenu();
					nl = sc.nextLine();
					try {
						option = Integer.parseInt(nl);
					} catch (NumberFormatException ex) {
						validInput = false;
						System.out.println("\n***please enter numbers only***");
						updateMenu();
						nl = sc.nextLine();
					}
				} while (!validInput);
				updateOption(service, option);
			} while (option != 0);
			// TODO back to main menu
		} else {
			System.out.println("\n***service id not valid***");
		}
	}

	/*
	 * take as input service and option then update information
	 */
	public void updateOption(Service service, int option) {
		switch (option) {
			case 0:
				System.out.println("\n***update***\n");
				System.out.println(service);
				System.out.println("*end of update*");
				break;
			// update name
			case 1:
				String name = service.getName();
				do {
					validInput = true;
					System.out.println("\nenter new name : ");
					name = sc.nextLine();
					if (name.length() == 0 || name.length() > 20) {
						validInput = false;
						System.out.println("\n***min 1 character, max 20 character***");
					}
				} while (!validInput);
				service.setName(name);
				break;
			// update start date
			case 2:
				int start = service.getStartDate();
				do {
					validInput = true;
					System.out.println("\nenter new start date : ");
					try {
						nl = sc.nextLine();
						start = Integer.parseInt(nl);
						if (nl.length() != 4) {
							validInput = false;
							System.out.println("\n***please enter valid start date(MMDD)***");
						}
					} catch (NumberFormatException ex) {
						validInput = false;
						System.out.println("\n***please enter numbers only***");
					}
				} while (!validInput);
				service.setStartDate(start);
				break;
			// update end date
			case 3:
				int end = service.getEndDate();
				do {
					validInput = true;
					System.out.println("\nenter new end date : ");
					try {
						nl = sc.nextLine();
						end = Integer.parseInt(nl);
						if (nl.length() != 4) {
							validInput = false;
							System.out.println("\n***please enter valid end date(MMDD)***");
						}
					} catch (NumberFormatException ex) {
						validInput = false;
						System.out.println("\n***please enter numbers only***");
					}
				} while (!validInput);
				service.setEndDate(end);
				break;
			// update description
			case 4:
				String comment = service.getDescription();
				do {
					validInput = true;
					System.out.println("\nenter new description : ");
					comment = sc.nextLine();
					if (comment.length() > 100) {
						validInput = false;
						System.out.println("\n***max 100 characters***");
					}
				} while (!validInput);

				service.setDescription(comment);
				break;
			// update fee
			case 5:
				float fee = service.getFees();
				do {
					validInput = true;
					System.out.println("\nenter new fee : ");
					try {
						nl = sc.nextLine();
						fee = Float.parseFloat(nl);
						if (fee < 0) {
							validInput = false;
							System.out.println("\n***please enter positive fee***");
						}
					} catch (NumberFormatException ex) {
						validInput = false;
						System.out.println("\n***please enter numbers only***");
					}
				} while (!validInput);
				service.setFees(fee);
				break;
			// update capacity
			case 6:
				int cap = service.getCapacity();
				do {
					validInput = true;
					System.out.println("\nenter new capacity : ");
					try {
						nl = sc.nextLine();
						cap = Integer.parseInt(nl);
						if (cap <= 0) {
							validInput = false;
							System.out.println("\n***please enter positive capacity***");
						}
					} catch (NumberFormatException ex) {
						validInput = false;
						System.out.println("\n***please enter numbers only***");
					}
				} while (!validInput);
				service.setCapacity(cap);
				break;
			// create a new session
			case 7:
				// input session day
				int day = 0;
				do {
					validInput = true;
					System.out.println("\nenter new session day : ");
					try {
						nl = sc.nextLine();
						day = Integer.parseInt(nl);
						if (day < 1 || day > 7) {
							validInput = false;
							System.out.println("\n***please enter valid day***");
							System.out.println("1 : MONDAY - 7 : SUNDAY");
						}
					} catch (NumberFormatException ex) {
						validInput = false;
						System.out.println("\n***please enter numbers day***");
						System.out.println("1 : MONDAY - 7 : SUNDAY");
					}
				} while (!validInput);
				// input session schedule
				System.out.println("\nenter new session schedule : ");
				String schedule = sc.nextLine();
				// create session
				Session newSession = new Session(service, day, schedule);
				// add session in service
				service.addSession(newSession);
				System.out.println(newSession.getSessionid() + " session added.\nSession info : ");
				System.out.println(newSession);
				break;
			// delete a session
			case 8:
				int sessionid = 0;
				do {
					validInput = true;
					System.out.println("\neneter session id to be deleted or enter 9 to cancel: ");
					nl = sc.nextLine();
					try {
						sessionid = Integer.parseInt(nl);
						if (nl.length() != 7 && sessionid != 9) {
							validInput = false;
							System.out.println("\n***please enter valid session ID or enter 9 to cancel***");
						}
					} catch (NumberFormatException ex) {
						validInput = false;
						System.out.println("\n***please enter numbers only***");
					}
				} while (!validInput);
				if (sessionid == 9) {
					System.out.println("\ncanceled\n");
					break;
				}
				// get session
				Session session = service.getSession(sessionid);
				if (session != null) {
					service.removeSession(session);
					System.out.println(session.getSessionid() + " session removed.\n");
					break;
				} else {
					System.out.println("\n***session not found.***\n");
					break;
				}
			default:
				System.out.println("\n***please enter a valid option***");
		}
	}

	/*
	 * take input service ID to find and delete corresponding service return false
	 * if the service does not exist
	 */
	public boolean deleteService(String serviceid) {
		Service service = getService(serviceid);
		if (service == null)
			return false;
		Delete(serviceid);
		return true;
	}

	/*
	 * second method called in subscribe() shows all the services available, and see
	 * if they have any available session
	 */
	public String findService() {
		int code = 9;
		do {
			validInput = true;
			System.out.println("\nenter service code, or enter 9 to cancel : ");
			nl = sc.nextLine();
			try {
				code = Integer.parseInt(nl);
				if (code == 9) {
					System.out.println("\ncanceled\n");
					return null;
				}
			} catch (NumberFormatException ex) {
				validInput = false;
				System.out.println("\n***please enter numbers only***");
			}
		} while (!validInput);

		String code2String = String.valueOf(code);
		boolean found = showSessionsToday(code2String);
		if (!found) {
			System.out.println("\ncanceled\n");
			return null;
		}
		return code2String;
	}

	/*
	 * third method called in subscribe() ask user to key in valid session ID and
	 * returns corresponding session
	 */
	public Session selectSession(String serviceCode) {
		int sessionID = 0;
		Service service = getService(serviceCode);
		Session session = null;
		do {
			validInput = true;
			System.out.println("\nenter session ID interested, or enter 9 to cancel : ");
			nl = sc.nextLine();
			try {
				sessionID = Integer.parseInt(nl);
				if (sessionID == 9) {
					System.out.println("\ncanceled\n");
					return null;
				}
				// check if the session exists
				session = service.getSession(sessionID);
				if (session == null) {
					validInput = false;
					System.out.println("\n***please enter valid session ID, or enter 9 to cancel***");
				}
				// check if session is full
				if (!session.subScribe()) {
					System.out.println("\nthe session you chose is full! Sign up fast next time!\n");
					System.out.println("\ncanceled\n");
					return null;
				}
			} catch (NumberFormatException ex) {
				validInput = false;
				System.out.println("\n***please enter numbers only***");
			}
		} while (!validInput);
		return session;
	}

	/*
	 * last method called in subscribe() at this time the session has already been
	 * chosen return true only if a valid member (with valid member ID) confirms
	 * subscription
	 */
	public boolean confirmSubscription(Session session) {
		System.out.println("\nSession chosen : ");
		System.out.println(session);

		int memberID = 0;
		do {
			validInput = true;
			System.out.println("\nplease enter member ID to confirm or enter 9 to cancel : ");
			nl = sc.nextLine();
			try {
				memberID = Integer.parseInt(nl);
				if (memberID == 9) {
					System.out.println("\ncanceled\n");
					return false;
				}
			} catch (NumberFormatException ex) {
				validInput = false;
				System.out.println("\n***please enter numbers only***");
			}
			// TODO add authenticate member method
		} while (!validInput);

		// user needs to key 1 to confirm
		// otherwise subscription is canceled anyways
		int ans = 0;
		System.out.println("\nenter 1 to confirm subscription : ");
		nl = sc.nextLine();
		try {
			ans = Integer.parseInt(nl);
		} catch (NumberFormatException ex) {
			System.out.println("\ncanceled\n");
			return false;
		}
		if (ans == 1) {
			boolean approved = session.addParticipant(memberID);
			if (approved) {
				System.out.println("\n" + memberID + " successfully subscribed to : ");
				System.out.println(session);
				System.out.println("fee : " + session.getFee());
				return true;
			} else {
				System.out.println("\n" + memberID + " has already subscribed to : ");
				System.out.println(session);
			}
		}
		System.out.println("\ncanceled\n");
		return false;
	}

	/*
	 * series of methods to subscribe to a session only return true if subscription
	 * is successful
	 */
	public boolean subscribe() {
		if (!showServices())
			return false;
		String code = findService();
		if (code == null)
			return false;
		Session session = selectSession(code);
		if (session == null)
			return false;
		if (confirmSubscription(session))
			return true;
		return false;
	}

	/*
	 * method to consult participants of a session user needs to key in valid
	 * session ID to get participants information
	 */
	public void consultParticipants() {
		int sessionID = 0;
		String result = null;
		do {
			validInput = true;
			System.out.print("\nenter session ID or enter 9 to cancel : ");
			nl = sc.nextLine();
			try {
				sessionID = Integer.parseInt(nl);
				if (nl.length() != 7 && sessionID != 9) {
					validInput = false;
					System.out.println("\n***please enter valid session ID or enter 9 to cancel***");
				}
			} catch (NumberFormatException ex) {
				validInput = false;
				System.out.println("\n***please enter numbers only***");
			}
			if (sessionID == 9) {
				System.out.println("\ncanceled\n");
				return;
			}
		} while (!validInput);
		result = showParticipants(sessionID);
		System.out.println(result);
	}
}