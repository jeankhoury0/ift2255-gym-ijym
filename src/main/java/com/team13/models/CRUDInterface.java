package com.team13.models;

/**
 * For the read update delete of the different objects
 * 
 * @author JeanKhoury
 */

public interface CRUDInterface<T> {
    T create(T obj);

    T read(String UUID);

    T update(T obj);

    boolean delete(String UUID);
}
