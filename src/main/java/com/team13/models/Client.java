package com.team13.models;

/**
 * Client Is what we used to consider member before
 * 
 * @author JeanKhoury
 * @version 1.0
 */
public class Client extends Member {

    private boolean hasPaid;

    public boolean getHasPaid() {
        return hasPaid;
    }

    public void setHasPaid(boolean hasPaid) {
        this.hasPaid = hasPaid;
    }
}
