package com.team13.models;

import java.util.ArrayList;
import java.util.List;

/**
 * ClientRepository Is the repository where all client are Stored. It contains
 * the create, read, update, delete actions
 */
public class ClientRepository implements CRUDInterface<Client> {

	private static List<Client> allClients = new ArrayList<Client>();

	@Override

	/**
	 * Check if the email already exist and if not create new client Test idea to
	 * try getting 2 same email
	 * 
	 * @param obj is the client <b>object</b>
	 * @return client Object if true null Object if already exist
	 */
	public Client create(Client obj) {
		if (userExist(obj) == false) {
			obj.newUID();
			allClients.add(obj);
			return obj;
		} else {
			System.out.println("There is already someone with the same email, please try again");
			return null;
		}
	}

	@Override
	/**
	 * return the client object
	 * 
	 * @param UUID is the memberID we are trying to search for
	 * @return the Client object
	 */
	public Client read(String UUID) {
		return searchClientByID(UUID);
	}

	@Override
	public Client update(Client obj) {
		// Take a client and replace it by another client
		return null;
	}

	@Override
	public boolean delete(String UUID) {
		try {
			Client client = searchClientByID(UUID);
			if (client != null) {
				allClients.remove(client);
				return true;
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return false;
	}

	private boolean userExist(Client obj) {
		// check if email is already registered
		if (allClients.isEmpty()) {
			return false;
		} else {
			for (Client client : allClients) {
				if (obj.getEmail().equalsIgnoreCase(client.getEmail())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Search in the repository
	 * 
	 * @param searchingUID search for the UID
	 * @return the client object
	 */

	public Client searchClientByID(String searchingUID) {
		try {
			if (searchingUID.length() != 10) {
				System.out.println("shoud be nine (ex: 1000000001)");
				return null;
			}
			for (Client client : allClients) {
				if (client.getuID() == Integer.parseInt(searchingUID)) {
					return client;
				}
			}
			System.out.println("No match where found");
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Client> getAllClients() {
		return allClients;
	}

}
