package com.team13.models;

/**
 * <h1>Member</h1> This class is to define the memberID info that is shared
 * across the professional and the member (activated)
 * 
 * @author JeanKhoury
 * @version 1.0
 * 
 */
public abstract class Member extends MemberInfo {
	private static int currentUID = 1000000001;
	private int uID;

	// check if uID is already allocated to member if not we allocate
	// the UID
	public int newUID() {
		try {
			if (this.uID == 0) {
				currentUID += 1;
				uID = currentUID;
				return uID;
			} else {
				// if the ID has already been attributed
				return this.uID;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getuID() {
		return uID;
	}

}
