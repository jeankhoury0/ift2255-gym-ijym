package com.team13.models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * MemberInfo Class with attributes for member including the name, email and
 * address
 * 
 * @author Jean Khoury
 * @version 1.0
 */
public abstract class MemberInfo {

    @Override
    public String toString() {
        return "MemberInfo [address=" + address + ", city=" + city + ", email=" + email + ", name=" + name
                + ", postalCode=" + postalCode + ", province=" + province + "]";
    }

    // regex for use for validation pattern
    private static Pattern EMAIL_REGEX = Pattern.compile("^(.+)@(.+)$");
    private static Pattern CA_POSTAL_CODE_REGEX = Pattern.compile("^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$");

    private String name;
    private String email;
    private String address;
    private String city;
    private String province;
    private String postalCode;
    private double amount;

    // Getter and setter (Attention special implementation)
    public String getName() {
        return name;
    }

    /**
     * setName check Name against the business rules and if it saved return true,
     * else will return a false
     * 
     * @param name is the name to be set to the client
     */
    public boolean setName(String name) {
        try {
            if (this.isNameValid(name) == true) {
                this.name = name;
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // if nothing is wrong return false
        return false;
    }

    public String getAddress() {
        return address;
    }

    public boolean setAddress(String address) {
        try {
            if (this.isAddressValid(address) == true) {
                this.address = address;
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // if nothing is wrong return false
        return false;
    }

    public String getCity() {
        return city;
    }

    public boolean setCity(String city) {
        try {
            if (this.isCityValid(city) == true) {
                this.city = address;
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // if nothing is wrong return false
        return false;
    }

    public String getProvince() {
        return province;
    }

    public boolean setProvince(String province) {
        try {
            if (this.isProvinceValid(province) == true) {
                this.province = province;
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // if nothing is wrong return false
        return false;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public boolean setPostalCode(String postalCode) {
        try {
            if (this.isPostalCodeValid(postalCode) == true) {
                this.postalCode = postalCode;
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // if nothing is wrong return false
        return false;
    }

    // Field Validation

    /**
     * Used to check if email is existent with fb or
     * 
     * @param emailToValidate is the email we are trying to validate
     * @return true if email is valid and is valid FB user, else will return false
     */
    private boolean isEmailValid(String emailToValidate) {
        final Matcher matcher = EMAIL_REGEX.matcher(emailToValidate);
        if (matcher.matches() == true) {
            return true;
        } else {
            return false;
        }
    }

    public String getEmail() {
        return email;
    }

    public boolean setEmail(String email) {
        try {
            if (this.isEmailValid(email) == true) {
                this.email = email;
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // if nothing is wrong return false
        return false;
    }

    /**
     * Used to check if name is strictly between 0 and 25 char Name with 0 char
     * should not be accepted
     * 
     * @param nameToValidate is the name we are trying to validate
     * @return true if email is valid, else will return false
     */
    private boolean isNameValid(String nameToValidate) {
        if (nameToValidate.length() > 0 && nameToValidate.length() <= 25) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Used to check if address is strictly between 0 and 25 char Address with 0
     * char should not be accepted
     * 
     * @param adrressToValidate is the address we are trying to validate
     * @return true if address is valid else will return false
     */
    private boolean isAddressValid(String addressToValidate) {
        if (addressToValidate.length() > 0 && addressToValidate.length() <= 25) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Used to check if city is strictly between 0 and 14 char City with 0 char
     * should not be accepted
     * 
     * @param cityToValidate is the city we are trying to validate
     * @return true if city is valid false for other cases
     */
    private boolean isCityValid(String cityToValidate) {
        if (cityToValidate.length() > 0 && cityToValidate.length() <= 14) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isProvinceValid(String provinceToValidate) {
        if (provinceToValidate.toString().length() == 2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Used to validate a postal code. A postal code in canada should have the
     * following: - in the format A1A 1A1, where A is a letter and 1 is a digit. - a
     * space separates the third and fourth characters. - do not include the letters
     * D, F, I, O, Q or U. - the first position does not make use of the letters W
     * or Z.
     * 
     * @param postalCodeToValidate ia the postal code we are trying to validate
     * @return true if PO is valid, else false
     */
    private boolean isPostalCodeValid(String postalCodeToValidate) {
        final Matcher matcher = CA_POSTAL_CODE_REGEX.matcher(postalCodeToValidate);
        if (matcher.matches() == true) {
            return true;
        } else {
            return false;
        }
    }

    public double getAmount() {

        return amount;
    }

    public void setAmount(double amount) {

        this.amount = amount;

    }

}
