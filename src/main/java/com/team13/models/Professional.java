package com.team13.models;

/**
 * Professional Is an extension of Member class where
 * 
 * @author JeanKhoury
 */
public class Professional extends Member {

    private String profession;

    public String getProfession() {
        return profession;
    }

    public boolean setProfession(String profession) {
        try {
            if (this.isProfessionValid(profession) == true) {
                this.profession = profession;
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // if nothing is wrong return false
        return false;
    }

    private boolean isProfessionValid(String professionToValidate) {
        if (professionToValidate.length() > 0 && professionToValidate.length() <= 25) {
            return true;
        } else {
            return false;
        }
    }
}
