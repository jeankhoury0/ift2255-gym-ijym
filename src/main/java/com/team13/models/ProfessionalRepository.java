package com.team13.models;

import java.util.ArrayList;
import java.util.List;

public class ProfessionalRepository implements CRUDInterface<Professional> {

	private static List<Professional> allProfessionals = new ArrayList<Professional>();

	@Override

	/*
	 * Check if the email already exist and if not create new professional Test idea
	 * to try getting 2 same email
	 */
	public Professional create(Professional obj) {
		if (userExist(obj) == false) {
			obj.newUID();
			allProfessionals.add(obj);
		} else {
			System.out.println("There is already someone with the same email");
		}
		return obj;
	}

	@Override
	/**
	 * return the professional object
	 * 
	 * @param UUID is the memberID we are trying to search for
	 * @return the Professional object
	 */
	public Professional read(String UUID) {
		return searchProfessionalByID(UUID);
	}

	@Override
	public Professional update(Professional obj) {
		// TODO Auto-generated method stub
		return null;

	}

	@Override
	public boolean delete(String UUID) {
		try {
			Professional professional = searchProfessionalByID(UUID);
			if (professional != null) {
				allProfessionals.remove(professional);
			}
			return true;

		} catch (Exception e) {
			e.getStackTrace();
		}
		return false;
	}

	private boolean userExist(Professional obj) {
		// check if email is already registered
		if (allProfessionals.isEmpty()) {
			return false;
		} else {
			for (Professional professional : allProfessionals) {
				if (obj.getEmail().equalsIgnoreCase(professional.getEmail())) {
					return true;
				}
			}
		}
		return false;
	}

	public Professional searchProfessionalByID(String searchingUID) {
		try {
			if (searchingUID.length() != 10) {
				System.out.println("shoud be nine (ex: 1000000001)");
				return null;
			}
			for (Professional professional : allProfessionals) {
				if (professional.getuID() == Integer.parseInt(searchingUID)) {
					return professional;
				}
			}
			System.out.println("No match where found");
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Professional> getAllProfessionals() {
		return allProfessionals;
	}

}
