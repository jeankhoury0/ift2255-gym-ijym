package com.team13.models;

/**
 * Province
 * @since 2011
 */
public enum Province {
    NL,
    PE,
    NS,
    NB,
    QC,
    ON,
    MB,
    SK,
    AB,
    BC,
    YT,
    NT,
    NU,
}