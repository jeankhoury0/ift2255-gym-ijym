package com.team13.models;

import java.io.FileWriter;

import com.team13.App;

public class ReportClient {

    static void generateReport() {
        try {
            FileWriter file = new FileWriter("ClientReport.dat"); // initialize File object
            checkEachMemeber(file);
        } catch (Exception e) {
            System.err.println("FIle could not be created");
            e.printStackTrace();
        }
    }

    static private void checkEachMemeber(FileWriter file) {

        for (Member m : App.allClient.getAllClients()) {
            if (m.getAmount() > 0) {
                try {
                    file.write(m.getName() + "\t" + m.getuID() + "\t" + m.getAddress() + "\t" + m.getCity() + "\t"
                            + m.getProvince() + "\t" + m.getPostalCode() + "\t");

                    // here we should write a a for loop for each service provided to the client
                    // TO DO
                } catch (Exception e) {
                    System.err.println("New Line could not be added to the file.");
                    e.printStackTrace();
                }
            }

        }

    }

}
