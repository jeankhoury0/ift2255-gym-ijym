package com.team13.models;

import java.io.FileWriter;

import com.team13.App;

public class ReportPro {

    static void generateReport() {
        try {
            FileWriter file = new FileWriter("ProfessionalReport.dat"); // initialize File object
            checkEachPro(file);
        } catch (Exception e) {
            System.err.println("FIle could not be created");
            e.printStackTrace();
        }
    }

    static private void checkEachPro(FileWriter file) {

        for (Professional p : App.allProfessional.getAllProfessionals()) {
            if (p.getAmount() > 0) {
                try {
                    file.write(p.getName() + "\t" + p.getuID() + "\t" + p.getAddress() + "\t" + p.getCity() + "\t"
                            + p.getProvince() + "\t" + p.getPostalCode() + "\t");

                    // here we should write a a for loop for each service provided by the
                    // professional to the members
                    // TO DO
                } catch (Exception e) {
                    System.err.println("New Line could not be added to the file.");
                    e.printStackTrace();
                }
            }

        }

    }

}
