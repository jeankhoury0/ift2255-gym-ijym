package com.team13.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Service Class for attributes of services
 * 
 * @author YC Chan
 */
public class Service {

	// regex for use for validation pattern
	private static Pattern DATE_JJMMAAAA_REGEX = Pattern.compile("^([0-9]+(-[0-9]+)+)$");
	private static Pattern TIME_HHMM_REGEX = Pattern.compile("^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$");

	public enum Day {
		MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
	}

	// ID generator
	private static int currentServiceID = 1000001;

	private String name;
	private Calendar actualDate;
	private Calendar startDate;
	private Calendar endDate;
	private Calendar serviceTime;
	private Day recuringDays;
	private int professionalID;
	private int maxCapacity;
	private int serviceID;
	private String comment;

	// is the array with all the sessions, get generated from the constructor
	ArrayList<Session> serviceSessions = new ArrayList<Session>();

	// Get set

	public String getName() {
		return name;
	}

	public boolean setName(String name) {
		if (isNameValid(name) == true) {
			this.name = name;
			return true;
		} else {
			return false;
		}
	}

	public Calendar getActualDate() {
		return actualDate;
	}

	public boolean setActualDate(String actualDate) {
		if (isDateValid(actualDate) == true) {
			this.actualDate = setToDateFormat(actualDate);
			return true;
		} else {
			return false;
		}
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public boolean setStartDate(String startDate) {
		if (isDateValid(startDate) == true) {
			this.startDate = setToDateFormat(startDate);
			return true;
		} else {
			return false;
		}
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public boolean setEndDate(String endDate) {
		if (isDateValid(endDate) == true) {
			this.endDate = setToDateFormat(endDate);
			return true;
		} else {
			return false;
		}
	}

	public Calendar getServiceTime() {
		return serviceTime;
	}

	public boolean setServiceTime(String serviceTime) {
		if (isTimeValid(serviceTime) == true) {
			this.serviceTime = setToHourFormat(serviceTime);
			return true;
		} else {
			return false;
		}
	}

	public Day getRecuringDays() {

		return recuringDays;
	}

	public boolean setRecuringDays(Day recuringDays) {

		this.recuringDays = recuringDays;
		return true;
	}

	public int getProfessionalID() {
		return professionalID;
	}

	public boolean setProfessionalID(int professionalID) {
		if (isProfessionalIDValid(professionalID) == true) {
			this.professionalID = professionalID;
			return true;
		} else {
			return false;
		}
	}

	public int getMaxCapacity() {
		return maxCapacity;
	}

	public boolean setMaxCapacity(int maxCapacity) {
		if (isMaxCapacityValid(maxCapacity) == true) {
			this.maxCapacity = maxCapacity;
			return true;
		} else {
			return false;
		}
	}

	public int getServiceID() {
		return serviceID;
	}

	public String getComment() {
		return comment;
	}

	public boolean setComment(String comment) {
		if (isCommentValid(comment) == true) {
			this.comment = comment;
			return true;
		} else {
			return false;
		}
	}

	// Validation
	private boolean isDateValid(String dateToValidate) {
		final Matcher matcher = DATE_JJMMAAAA_REGEX.matcher(dateToValidate);
		if (matcher.matches() == true) {
			return true;
		} else {
			return false;
		}
	}

	// Used to parse a String of format DD-MM-YYYY to a calendar format
	private Calendar setToDateFormat(String dateToParse) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date date;
		try {
			date = sdf.parse(dateToParse);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			return cal;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}

	}

	private boolean isTimeValid(String timeToValidate) {
		final Matcher matcher = TIME_HHMM_REGEX.matcher(timeToValidate);
		if (matcher.matches() == true) {
			return true;
		} else {
			return false;
		}
	}

	private Calendar setToHourFormat(String timeToParse) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:MM");
		Date date;
		try {
			date = sdf.parse(timeToParse);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			return cal;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	private boolean isMaxCapacityValid(int capacityToValidate) {
		if (capacityToValidate <= 30) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isProfessionalIDValid(int professionalIDToValidate) {
		if (String.valueOf(professionalIDToValidate).length() == 9) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isCommentValid(String commentToValidate) {
		if (commentToValidate.length() <= 100) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isNameValid(String nameToValidate) {
		if (nameToValidate.length() <= 20 && nameToValidate.length() > 0) {
			return true;
		} else {
			return false;
		}
	}

	private int generateServiceID() {
		if (this.serviceID == 0) {
			currentServiceID += 1;
			serviceID = currentServiceID;
			return this.serviceID;
		} else {
			// if the ID has already been attributed
			return this.serviceID;
		}
	}

	private void generateSession() {

		Calendar i = this.startDate;
		Long DaysBetween = daysBetween(i, endDate);
		while (DaysBetween <= 0) {
			// TODO add the generation
		}
	}

	private static long daysBetween(Calendar from, Calendar to) {
		// pulled from stack overflow
		// https://stackoverflow.com/questions/43411808/java-calendar-subtract-two-dates-to-get-difference-in-days-between-the-two
		return ChronoUnit.DAYS.between(from.toInstant(), to.toInstant());
	}

	/**
	 * Constrcutor for testing purpuses
	 * 
	 * @author JeanKhoury
	 */
	public Service() {
		generateServiceID();
		// generateSession();
	};

}