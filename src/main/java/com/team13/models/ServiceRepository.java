package com.team13.models;

import java.util.ArrayList;
import java.util.List;

public class ServiceRepository implements CRUDInterface<Service> {

	public static List<Service> allServices = new ArrayList<>();
	private static int SERVICE_NO = 100;

	/*
	 * take input session ID and find corresponding session return list of
	 * participants if session ID is valid
	 */
	public String showParticipants(int sessionID) {
		String serviceID = String.valueOf(sessionID).substring(0, 3);
		Service service = getService(serviceID);
		if (service == null) {
			serviceID += " is not valid\n";
			return serviceID;
		}
		Session session = service.getSession(sessionID);
		if (session == null) {
			serviceID = sessionID + " is not valid\n";
			return serviceID;
		}
		String participants = session.getParticipants();
		return participants;
	}

	/*
	 * print out all services in repository return true if there is at least one
	 * service
	 */
	public boolean showServices() {
		if (allServices.isEmpty()) {
			System.out.println("no service in repository");
			return false;
		} else {
			System.out.println("\nService Info : \n");
			for (Service service : allServices) {
				System.out.println(service);
				System.out.println();
			}
			return true;
		}
	}

	/*
	 * take input service ID and print out all available sessions today of the
	 * service return true only if there is at least 1 session
	 */
	public boolean showSessionsToday(String serviceid) {
		Service service = getService(serviceid);
		if (service != null) {
			if (service.isAvailable()) {
				ArrayList<Session> sessionsToday = service.getSessionsToday();
				if (sessionsToday == null) {
					System.out.println("\n***no session available yet***\n");
					return false;
				} else if (sessionsToday.isEmpty()) {
					System.out.println("\n***no session available today***\n");
					return false;
				} else {
					System.out.println("\nSession for " + service.getName() + " today : \n");
					for (Session session : sessionsToday) {
						System.out.println(session);
						System.out.println();
					}
					return true;
				}
			} else {
				System.out.println("\nService " + service.getName() + " is not available\n");
				return false;
			}
		} else {
			System.out.println("\n***service id not valid***\n");
			return false;
		}
	}

	/*
	 * take input service and check if the service exists already in repository by
	 * comparing the names of 2 services
	 */
	public boolean serviceExists(Service obj) {
		if (allServices.isEmpty()) {
			return false;
		} else {
			for (Service service : allServices) {
				if (obj.getName().equalsIgnoreCase(service.getName())) {
					return true;
				}
			}
			return false;
		}
	}

	/*
	 * take input service ID and return corresponding service
	 */
	public Service getService(String serviceid) {
		if (allServices.isEmpty())
			return null;
		for (Service service : allServices) {
			if (serviceid.equals(String.valueOf(service.getServiceID()))) {
				return service;
			}
		}
		return null;
	}

	/*
	 * take input service and check if it already exists in list if not add it to
	 * the list
	 */
	@Override
	public void create(Service obj) {
		if (!serviceExists(obj)) {
			allServices.add(obj);
			obj.setServiceID(SERVICE_NO);
			SERVICE_NO++;
			System.out.println();
			System.out.println(obj.getServiceID() + " added : \n");
			System.out.println(obj);
		} else {
			System.out.println("error.");
		}
	}

	@Override
	public Service read(String UUID) {
		Service serviceRead = getService(UUID);
		return serviceRead;
	}

	@Override
	public void update(Service obj) {

	}

	@Override
	/*
	 * take input service ID and delete the corresponding service from list no need
	 * to check if service ID is valid since it is done in ServiceControll
	 */
	public void delete(String UUID) {
		Service service2BD = getService(UUID);
		if (service2BD != null) {
			allServices.remove(service2BD);
			System.out.println(service2BD.getServiceID() + " " + service2BD.getName() + " deleted.\n\n");
		}
	}
}