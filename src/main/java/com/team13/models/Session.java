package com.team13.models;

import java.util.ArrayList;

public class Session {

	private Service service;
	private ArrayList<Member> participants = new ArrayList<Member>();

	public Session(Service service, int day, String schedule) {
		this.service = service;
		generateSessionID();
		this.day = toDay(day);
		this.schedule = schedule;
	}

	public enum Day {
		MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
	}

	private Day day;
	private String schedule;
	private int subscribed;
	private int sessionid;

	private double fee;

	public boolean isParticipant(int member) {
		if (participants.isEmpty()) {
			return false;
		} else {
			for (Member i : participants) {
				if (i.getuID() == member) {
					return true;
				}
			}
			return false;
		}
	}

	public boolean addParticipant(int member) {
		if (!isParticipant(member)) {
			Client x = new Client();
			participants.add(x);
			subscribed++;
			return true;
		} else {
			return false;
		}
	}

	/*
	 * return String containing all the session's participants info
	 */
	public String getParticipants() {
		String string = toString();
		if (participants.isEmpty()) {
			string += "\n*no participants for this session yet*";
			return string;
		}
		string += "\nParticipant(s) : \n";
		for (int i = 0; i < participants.size(); i++) {
			string += i + participants.get(i).getName() + "\n";
		}
		return string;

	}

	/*
	 * public ArrayList<Member> getParticipants() { return participants; }
	 */

	// check if subscription is available for this session
	public boolean subScribe() {
		if (this.getSubscribed() < service.getCapacity()) {
			return true;
		} else {
			return false;
		}
	}

	public Day toDay(int day) {
		switch (day) {
			case 1:
				return Day.MONDAY;
			case 2:
				return Day.TUESDAY;
			case 3:
				return Day.WEDNESDAY;
			case 4:
				return Day.THURSDAY;
			case 5:
				return Day.FRIDAY;
			case 6:
				return Day.SATURDAY;
			case 7:
				return Day.SUNDAY;
			default:
				return null;
		}
	}

	public int toInt(Day day) {
		switch (day) {
			case MONDAY:
				return 1;
			case TUESDAY:
				return 2;
			case WEDNESDAY:
				return 3;
			case THURSDAY:
				return 4;
			case FRIDAY:
				return 5;
			case SATURDAY:
				return 6;
			case SUNDAY:
				return 7;
			default:
				return -1;
		}
	}

	public Day getDay() {
		return day;
	}

	public int getDayInt() {
		return toInt(day);
	}

	public String toString() {
		String s = service.getName() + "\n" + day.name() + " " + schedule + "\n" + getSessionid();
		return s;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public int getSubscribed() {
		return subscribed;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public void generateSessionID() {
		String serviceCode = String.valueOf(service.getServiceID());
		String number = String.valueOf(service.getSessionNo());
		String profID = String.valueOf(service.getProfessionalID()).substring(7);
		String sessionID = serviceCode + number + profID;
		int session = Integer.parseInt(sessionID);
		this.sessionid = session;
		service.setSessionNo(service.getSessionNo() + 1);
	}

	public int getSessionid() {
		return sessionid;
	}

}
