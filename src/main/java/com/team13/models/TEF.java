package com.team13.models;

import com.team13.App;

import java.io.FileWriter;

/**
 * <h2>Generate TEF File</h2> To generate the TEF file Professional Name +
 * Professional Number + amount to transfer <br>
 * weekly basis
 */

public class TEF {

	static void generateTEF() {
		try {
			FileWriter file = new FileWriter("TEF.dat"); // initialize File object
			checkToPayProfessional(file);
		} catch (Exception e) {
			System.err.println("FIle could not be created");
			e.printStackTrace();
		}
	}

	static private void checkToPayProfessional(FileWriter file) {
		/**
		 * We want to check the professionals that have a non 0 balance to add them to
		 * the file
		 */
		for (Professional p : App.allProfessional.getAllProfessionals()) {
			if (p.getAmount() > 0) {
				try {
					file.write(p.getName() + "\t" + p.getuID() + "\t" + p.getAmount());
				} catch (Exception e) {
					System.err.println("New Line could not be added to the file.");
					e.printStackTrace();
				}
			}

		}

	}

}
