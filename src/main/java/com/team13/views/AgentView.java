package com.team13.views;

import java.util.Scanner;

import com.team13.App;
import com.team13.models.*;

/*
 * Mahdi 1- Create account a. Create a pro account instance (c) - Create a new
 * instancefo clietn - Collect all info, with validation eg: c.setName() if
 * true, go to next scanner, if false ask again - AT THE END OF ALL OF THIS -- u
 * add the objetct c to proRepo
 *
 * b. Create a client Account 2- Create a seervice
 *
 */

/**
 * AgentView The agent should be able to:
 * <li>open a new account</li>
 * <li>create a service session</li>
 */
public class AgentView {

    private static Scanner sc = new Scanner(System.in);

    protected static void agentMenu() {

        try {

            do {
                System.out.println("[1]Open a new account for Client");
                System.out.println("[2]Open a new account for Professional");
                System.out.println("[3]Create a service session");
                System.out.println("Enter your choice:");

                int choice = sc.nextInt();

                switch (choice) {
                    case 1:
                        openClientAccount();
                        break;
                    case 2:
                        openProfessionalAccount();

                        break;
                    case 3:
                        createService();
                        break;

                    default:
                        System.out.println("Invalid Choice, Please Enter 1, 2, 3 or 0 to exit");

                        break;
                }

            } while (true);

        } catch (Exception e) {
            e.printStackTrace();
            agentMenu();
        } finally {
            sc.close();

        }

    }

    private static String errorMessage = "! Sorry invalid input, please try again";

    private static void openClientAccount() {
        boolean businessRuleValid = false;
        // iniatilise new empty client
        Client newClient = new Client();
        sc.nextLine();

        do {
            System.out.println("Please Enter the client's name");
            String input = sc.nextLine();
            businessRuleValid = newClient.setName(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the client's E-mail");
            String input = sc.nextLine();
            businessRuleValid = newClient.setEmail(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the client's address");
            String input = sc.nextLine();
            businessRuleValid = newClient.setAddress(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the client's city");
            String input = sc.nextLine();
            businessRuleValid = newClient.setCity(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the client's province");
            String input = sc.nextLine();
            businessRuleValid = newClient.setProvince(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the client's postal code");
            String input = sc.nextLine();
            businessRuleValid = newClient.setPostalCode(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        newClient.newUID();
        newClient.setHasPaid(true);
        newClient.getuID();
        App.allClient.create(newClient);

    }

    private static void openProfessionalAccount() {
        boolean businessRuleValid = false;
        // iniatilise new empty client
        Professional newClient = new Professional();
        sc.nextLine();

        do {
            System.out.println("Please Enter the client's name");
            String input = sc.nextLine();
            businessRuleValid = newClient.setName(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the client's E-mail");
            String input = sc.nextLine();
            businessRuleValid = newClient.setEmail(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the client's address");
            String input = sc.nextLine();
            businessRuleValid = newClient.setAddress(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the client's city");
            String input = sc.nextLine();
            businessRuleValid = newClient.setCity(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the client's province");
            String input = sc.nextLine();
            businessRuleValid = newClient.setProvince(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the client's postal code");
            String input = sc.nextLine();
            businessRuleValid = newClient.setPostalCode(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the the professional profession");
            String input = sc.nextLine();
            businessRuleValid = newClient.setProfession(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        newClient.newUID();
        System.out.println("Your id: " + newClient.getuID());

        App.allProfessional.create(newClient);
    }

    private static void createService() {
        boolean businessRuleValid = false;
        // iniatilise new empty client
        Service newService = new Service();
        sc.nextLine();

        do {
            System.out.println("Please Enter the service's name");
            String input = sc.nextLine();
            businessRuleValid = newService.setName(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the service's start date (DD-MM-YYYY)");
            String input = sc.nextLine();
            businessRuleValid = newService.setStartDate(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the service's end date (DD-MM-YYYY)");
            String input = sc.nextLine();
            businessRuleValid = newService.setEndDate(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the service's time (HH:MM)");
            String input = sc.nextLine();
            businessRuleValid = newService.setServiceTime(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the service's max Capacity (max 30) ");
            int input = sc.nextInt();
            businessRuleValid = newService.setMaxCapacity(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter the service's professional's ID ");
            int input = sc.nextInt();
            businessRuleValid = newService.setProfessionalID(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        do {
            System.out.println("Please Enter a comment");
            String input = sc.nextLine();
            businessRuleValid = newService.setComment(input);
            if (businessRuleValid == false) {
                System.out.println(errorMessage);
            }
        } while (businessRuleValid != true);

        ServiceRepository.allServices.add(newService);

    }

}
