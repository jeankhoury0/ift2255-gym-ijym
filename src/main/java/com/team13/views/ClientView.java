package com.team13.views;

import java.util.Scanner;

import com.team13.App;
import com.team13.controller.ClientController;
import com.team13.models.Client;

public class ClientView {
    private static Scanner sc = new Scanner(System.in);
    private static Client client;

    /*
     * Client view should have: 1- updateClientInfo 2 - deleteClient 3- readinfo (to
     * string) 4- register for session (with should call registerToSession() and ask
     * all infos) 5- Enter Gym *check if client is valid active) - if valid return
     * sucess and qr code () - else return reason why invalid
     * 
     */

    protected static void ClientViewMain() {
        // Check if client ID is valid
        System.out.println("==============");
        System.out.println("CLIENT MENU");
        boolean state = false;
        while (state == false) {
            state = authentificate();
        }
        System.out.println(
                "[1] Enter gym \n" + "[2] register for a session \n" + "[3] check info \n " + "[4] update profile");
        int choice = sc.nextInt();
        switch (choice) {
            case 1:
                enterGym();
                break;
            case 2:
                // TODO registerSession();
                break;
            case 3:
                System.out.println("Check info");
                System.out.println(client.toString());
                break;
            default:
                System.out.println("Invalid information");
                break;
        }

    }

    private static boolean authentificate() {
        System.out.println("Please authentificate yourself");
        System.out.println("ID please");
        String input = sc.nextLine();
        // if response is not valid
        Client c = App.allClient.searchClientByID(input);
        if (c == null) {
            System.out.println("This client cannot be found");
            return false;
        } else {
            System.out.println("Authentification sucessfull");
            // set the client
            client = c;
            return true;
        }

    }

    private static void enterGym() {
        String response = ClientController.enterGym(client);

        if (response.contains("Valid")) {
            // if response is valid
            System.out.println();
            System.out.println(response);
            System.out.println("Name: " + client.getName());
            System.out.println("Member ID: " + client.getuID());
            System.out.println("QR code (will be generated in the real app)");
            System.out.println();
        } else {
            // cannot enter the gym and status is invalid
            System.out.println(response);
        }

    }

    public static void updateClient() {
    }

}