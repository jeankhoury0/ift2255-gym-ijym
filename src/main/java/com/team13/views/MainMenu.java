package com.team13.views;

import java.util.Scanner;

/**
 * Main menu is too invoke the principal options
 * 
 * @author Imane
 */

public class MainMenu {

    public static void displayMenu() {
        selectOption();
    }

    /**
     * Used to quit the app
     */
    private static void exit() {
        System.exit(0);
    }

    /**
     * Used to select an option
     */
    private static void selectOption() {
        Scanner sc = new Scanner(System.in);
        try {
            while (true) {
                System.out.println("Please select who you are \n" + "[1] Agent\n" + "[2] Manager\n" + "[3] Member\n"
                        + "[4] Professional\n" + "[0] To Quit");
                System.out.println("Enter your choice:");

                int choice = sc.nextInt();
                switch (choice) {
                    case 0:
                        exit();
                        break;
                    case 1:
                        AgentView.agentMenu();
                        break;
                    case 2:
                        // todo managerview;
                        break;
                    case 3:
                        ClientView.ClientViewMain();
                        break;
                    case 4:
                        ProView.professionalMainView();
                        break;
                    default:
                        System.out.println("Invalid Choice, Please Enter 1, 2, 3 or 0 to exit");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            selectOption();
        } finally {
            sc.close();
        }
    }

}
