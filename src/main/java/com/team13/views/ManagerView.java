package com.team13.views;

import java.util.Scanner;

import com.team13.models.TEF;

/**
 * ManagerView
 */
public class ManagerView {

    public void ManagerMenu() {
        Scanner sc = new Scanner(System.in);

        System.out.println("==============");
        System.out.println("MANAGER MENU");
        System.out.println("[1] Generate the repport for professional \n" + "[2] Generate the repport for client \n"
                + "[3] Generate TEF");
        int choice = sc.nextInt();
        switch (choice) {
            case 1:
                proReport();
                break;
            case 2:
                clientReport();
                break;
            case 3:
                generateTEF();
                break;
            default:
                System.out.println("Invalid information");
                break;
        }

    }

    private void generateTEF() {

    }

    private void clientReport() {

    }

    private void proReport() {
    }

}