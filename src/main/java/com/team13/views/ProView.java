package com.team13.views;

import java.util.Scanner;

import com.team13.App;
import com.team13.models.Professional;

public class ProView {
    /*
     * 1- Check attendance to seance *by session id* 2- Check qr code
     */

    private static Scanner sc = new Scanner(System.in);
    private static Professional professional;

    protected static void professionalMainView() {
        // Check if pro ID is valid
        System.out.println("==============");
        System.out.println("PRO MENU");
        boolean state = false;
        while (state == false) {
            state = authentificate();
        }
    }

    private static boolean authentificate() {
        System.out.println("Please authentificate yourself");
        System.out.println("ID please");
        String input = sc.nextLine();
        // if response is not valid
        Professional p = App.allProfessional.searchProfessionalByID(input);
        if (p == null) {
            System.out.println("This professional cannot be found");
            return false;
        } else {
            System.out.println("Authentification sucessfull");
            // set the client
            professional = p;
            return true;
        }

    }

    public void checkAttendanceByID() {

    }

    public void checkQrCode() {

    }

}