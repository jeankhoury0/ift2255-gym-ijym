package com.team13.views;

import com.team13.App;
import com.team13.models.Client;
import com.team13.models.Professional;

/**
 * dumbData
 */
public class dumbData {

    public static void generateData() {
        Client c1 = new Client();
        Client c2 = new Client();
        Client c3 = new Client();

        c1.setName("John");
        c1.setAddress("123 Rainbow str");
        c1.setEmail("Johndoe1@gmail.com");
        c1.setHasPaid(true);
        c1.setPostalCode("H1N3F1");
        c1.setCity("Raibow city");
        c1.newUID();

        App.allClient.create(c1);

        c2.setName("Jane");
        c2.setAddress("123 Rainbow ave");
        c2.setEmail("JaneDoe1@gmail.com");
        c2.setHasPaid(true);
        c2.setPostalCode("H3N2F1");
        c2.setCity("Raibow city");
        c2.newUID();

        App.allClient.create(c2);

        c3.setName("Jeanine");
        c3.setAddress("1234 Rainbow ave");
        c3.setEmail("JaneDoe2@gmail.com");
        c3.setHasPaid(true);
        c3.setPostalCode("H3N2D1");
        c3.setCity("Montreal");
        c3.newUID();

        App.allClient.create(c3);

        // generate pro

        Professional p1 = new Professional();
        Professional p2 = new Professional();
        Professional p3 = new Professional();

        p1.setName("Jeanine");
        p1.setAddress("1234 Rainbow ave");
        p1.setEmail("JaneDoe3@gmail.com");
        p1.setProfession("Dentist");
        p1.setPostalCode("H3N2D1");
        p1.setCity("Montreal");
        p1.newUID();
        App.allProfessional.create(p1);

        p2.setName("Jeanine");
        p2.setAddress("1234 Rainbow ave");
        p2.setEmail("JaneDoe5@gmail.com");
        p2.setProfession("Roling");
        p2.setPostalCode("H3N2D1");
        p2.setCity("Montreal");
        p2.newUID();

        App.allProfessional.create(p2);

        p3.setName("Jeanine");
        p3.setAddress("1234 Rainbow ave");
        p3.setEmail("JaneDoe7@gmail.com");
        p3.setProfession("test");
        p3.setPostalCode("H3N2D1");
        p3.setCity("Montreal");
        p3.newUID();

        App.allProfessional.create(p3);

    }

}