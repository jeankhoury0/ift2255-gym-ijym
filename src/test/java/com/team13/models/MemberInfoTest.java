package com.team13.models;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class MemberInfoTest {

    @Test
    public void testClientCreation(){
        Client c = new Client();
        assertFalse("Failed because the name is empty", c.setName(""));
        assertFalse("echec.nom sup a 25 caract", c.setName("Imane long nom de famille tres long ne devrait pas appassert"));
        assertTrue("echec. nom devrait passer", c.setName("IMANE BASSINI"));
        
        assertFalse("echec.email incorect", c.setEmail("imane.com"));
        assertTrue("echec.email correct", c.setEmail("imane.bassini@umontreal.ca"));

        c.newUID();
        assertFalse("echec.UID incorect less then 9 numb ", String.valueOf(c.getuID()).length() == 9);

        assertFalse("echec. because the adress is empty", c.setAddress(""));
        assertFalse("echec.adress sup to 25 caract ", c.setAddress("2780 place darllington loin de la vraie place ou imane habite vraiment "));
        assertTrue("echec.adress must be passed", c.setAddress("2780 place darlington"));

        assertFalse("echec.because the city is empty", c.setCity(""));
        assertFalse("echec.because city is sup to 14 caracter", c.setCity("montrealquebectorontolaval"));
        assertTrue("echec.city correct", c.setCity("Montreal"));

        assertFalse("echec.province sup to 2 caracter ", c.setProvince("Quebec"));
        assertFalse("echec.province  1 caracter ", c.setProvince("Q"));
        assertFalse("echec.province is empty ", c.setProvince(""));
        assertTrue("echec.province correct", c.setProvince("Qc"));

        assertFalse("echec.postal code sup to 6 caract", c.setPostalCode("H3s1l52"));
        assertFalse("echec.postal code less than 6 caract ", c.setPostalCode("H3S1L"));
        assertTrue("echec.postal code correct ", c.setPostalCode("H3S1L5"));
        assertFalse("echec.postal code vide", c.setPostalCode(""));
    }
}
