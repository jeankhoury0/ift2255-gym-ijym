package com.team13.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ServiceRepositoryTest {
    @Test
    public void testCreate() {

    // Date de début du service (JJ-MM-AAAA)
    // Date de fin du service (JJ-MM-AAAA)
    // Heure du service (HH:MM)
    // Récurrence hebdomadaire du service (quels jours il est offert à la même heure) todo
    // Capacité maximale (maximum 30 inscriptions)
    // Code de la séance (7 chiffres)
    // nom de la sesance (20 char)
    // Commentaires (100 caractères) (facultatif).

        
        //Service s = new Service(professional, name, startDate, endDate, description, fees, capacity);
        Service s = new Service();
        
    
        
        // assertTrue("Failed because of startDate false even with correct format", s.setStartDate("12-02-2022"));
        // assertFalse("Failed because of startDate not corect even with correct format", s.setStartDate("12-13-2022"));
        // assertTrue("Failed.startDate is correct", s.setStartDate("09-03-2022"));


        // assertTrue("Failed because of endDate false even with correct format", s.setEndDate("12-22-2022"));
        // assertTrue("Failed because of endDate false even with correct format", s.setEndDate("10-02-2022"));
        // assertFalse("Failed because of endDate false even with correct format", s.setEndDate("102-02-2022"));
        // assertFalse("Failed because of endDate false even with correct format", s.setEndDate("10-022-2022"));
        // assertFalse("Failed because of endDate false even with correct format", s.setEndDate("10-02-20223"));
        assertTrue("Failed.endDate is correct", s.setEndDate("30-03-2022"));

        assertFalse("Failed.service time not in correct format", s.setServiceTime("23:023"));
        assertFalse("Failed.service time false even with correct format", s.setServiceTime("23:60"));
        assertFalse("Failed.service time not in correct format", s.setServiceTime("023:23"));
        assertTrue("Failed.service time in correct format", s.setServiceTime("23:23"));
        
        assertFalse("Failed.maxCapacity sup to 30 inscriptions", s.setMaxCapacity(31));
        assertTrue("Failed.maxCapacity equal 30 inscriptions", s.setMaxCapacity(30));
        assertTrue("Failed.maxCapacity less than 30 inscriptions", s.setMaxCapacity(0));
        assertTrue("Failed.maxCapacity less than 30 inscriptions", s.setMaxCapacity(25));


        assertTrue("Failed. ServiceID less than 7 numb", String.valueOf(s.getServiceID()).length() == 7 );

        assertFalse("Failed.name sup to 20 char",s.setName("zumbaavecleprofessionel qui ne vq pas donner zumba ce soir"));
        assertFalse("Failed.name is empty ",s.setName(""));
        assertTrue("Failed.name is correct ",s.setName("Zumba"));

        assertTrue("Comment is empty ", s.setComment(""));
        assertFalse("Comment less than 101 char ", s.setComment(" Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"));
        assertTrue("Comment sup to 100 char ", s.setComment("Just perfect"));    
    }

    @Test
    public void testShowSessionsToday() {

    }
}
